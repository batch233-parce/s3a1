USE blog_db;

-- Users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", CAST('21-01-01 01:00:00' AS DateTime));
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", CAST('21-01-01 02:00:00' AS DateTime));
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", CAST('21-01-01 03:00:00' AS DateTime));
INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", CAST('21-01-01 04:00:00' AS DateTime));
INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", CAST('21-01-01 05:00:00' AS DateTime));

-- Posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", CAST('21-01-02 01:00:00' AS DateTime));
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", CAST('21-01-02 02:00:00' AS DateTime));
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (3, "Third Code", "Welcome to Mars!", CAST('21-01-02 03:00:00' AS DateTime));
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solor system!", CAST('21-01-02 04:00:00' AS DateTime));

-- Get all posts with an Author ID of 1
SELECT * FROM posts WHERE author_id = 1;

-- Get user emails and datetime of creation
SELECT email, datetime_created FROM users;

-- Update a post content using the record's id
UPDATE posts SET content = "Hello to the people of the Earth" WHERE id = 2;

-- Delete user using email
DELETE FROM users WHERE email = "johndoe@gmail.com";